// problem5
// The car lot manager needs to find out
//  how many cars are older than the year 2000. 
//  Using the array you just obtained from the
//   previous problem, find out how many 
// cars were made before the year 2000 and 
// return the array of older cars and log its length.
function problem5(inventory){
    let carYears = [];
    let oldCars=[]
    for (let i = 0; i < inventory.length; i++) {
        carYears.push(inventory[i].car_year);
        
        if (carYears[i] < 2000) {
            oldCars.push(carYears[i]);
            console.log(oldCars)
        }
    }
    console.log(oldCars.length)
}
module.exports=problem5